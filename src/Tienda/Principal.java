package Tienda;
import java.io.IOException;
import java.util.*;
 
public class Principal {
 
    public static void main(String[] args) throws IOException{
        //Creamos una nueva lista:
        ArrayList<Electrodomesticos> electrodomestic = new ArrayList<>(0);
        //Variables:
        int opc = 0, tam = 0;
        tam = Apoyo.tamArchivo(electrodomestic);
        //Metodo while:
        while(opc != 3) {
            opc = Apoyo.menu();
 
            switch(opc) {
                case 1:
                    Apoyo.leerArchivo(electrodomestic);
                    break;
 
                case 2:
                    Apoyo.mostrar(electrodomestic);
                    break;
            }
        }
    }
}