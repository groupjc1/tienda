package Tienda;
 
public class Lavadora extends Electrodomesticos{
    //Atributos:
    private int cargaLavadora = 5;
 
    //Constructor:
    public Lavadora(int carLavadora, double pre, int consu, int pe) {
        super(pre, consu, pe);
        cargaLavadora = carLavadora;
    }
    //Metodo que muestre los datos:
    public void mostrar(){
        System.out.println("-------------------------------------");
        System.out.println("Tipo de Electrodomestico (1:Lavadora)");
        System.out.println("Precio Base: " + getPrecioBase());
        System.out.println("Consumo Electrico: " + getConsumoElectrico());
        System.out.println("Peso: " + getPeso() + "Kg");
        System.out.println("Carga: " + cargaLavadora + "Kg");
        System.out.println("Precio final: " + precioFinal() + " Bs.S");
        System.out.println();
    }
    //calcular pago:
    public double precioFinal(){
        //Variables: Invocamos el método precioFinal de la calse Electrodomestico
        double monto = super.precioFinal();
        //Creamos el condicional if:
        if (cargaLavadora > 30 ) {
            monto += 50;
        }
        return monto;
    }
}