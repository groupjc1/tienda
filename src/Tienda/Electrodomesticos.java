package Tienda;
 
public abstract class Electrodomesticos {
    //Atributos:
    protected double precioBase;
    protected int consumoElectrico;
    protected int peso;
 
    //Constructor:
    public Electrodomesticos(double pre, int consu, int pe) {
        precioBase = pre;
        consumoElectrico = consu;
        peso = pe;
    }
    //Declaramos los metodos get:
    public double getPrecioBase() {
        return precioBase;
    }
 
    public int getConsumoElectrico() {
        return consumoElectrico;
    }
 
    public int getPeso() {
        return peso;
    }
    //calcular pago:
    public double precioFinal(){
        //Variables:
        double monto = 0;
        //Creamos un switch para que determine el tipo de consumo electrico 
        //(si es tipo 1 al monto se le suma 100, si es tipo 4 se le suma 50):
        switch (consumoElectrico) {
            case 1:
                monto += 100;
                break;
            case 2:
                monto += 80;
                break;
            case 3:
                monto += 60;
                break;
            case 4:
                monto += 50;
                break;
        }
        //Creamos el metodo if, el cual permitira que identifiquemos que digitos
        //son de "Peso". Ademas, con el if calcularemos el monto del peso segun 
        //la tabla "Peso del aparato"
        if (peso >= 0 && peso <= 19) {
            monto += 10;
        } else if (peso == 20 && peso <= 49) {
            monto += 50;
        } else if (peso == 50 && peso <= 79) {
            monto += 80;
        }else if (peso > 79) {
            monto += 100;
        }
        //Creamos un return, donde regresara el valor del precio base y lo sumara
        //con el monto calculado de acuerdo a lo establecido en el ejercicio.
        return precioBase + monto;
    }
    //Mostramos los datos a recibir:
    public abstract void mostrar();
}